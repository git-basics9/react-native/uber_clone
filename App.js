import HomeScreen from './components/HomeScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MapScreen from './components/MapScreen';
import EatsScreen from './components/EatsScreen';
import { Provider } from 'react-redux';
import {store} from './components/store/store'
import React from 'react';
    import {
      KeyboardAvoidingView,
      Platform,
    } from 'react-native';
 
const App = () => {
  const Stack = createNativeStackNavigator();
  return (
    
    <Provider store={store}>
    <NavigationContainer>
        <KeyboardAvoidingView
        behavior={Platform.OS==="ios"?"padding":"height"}
        style={{flex:1}}
        enabled
        >
      <Stack.Navigator>
      <Stack.Screen name="HomeScreen" component={HomeScreen}
      options={{
      headerShown:false
      }}/>
      <Stack.Screen name="MapScreen" component={MapScreen}
      options={{
      headerShown:false
      }}/>
      <Stack.Screen name="EatsScreen" component={EatsScreen}
      options={{
      headerShown:false
      }}/>
      </Stack.Navigator>
        </KeyboardAvoidingView>
    </NavigationContainer>
    </Provider>
  );
};


export default App;


// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   inner: {
//     padding: 24,
//     flex: 1,
//     justifyContent: 'space-around',
//   },
//   header: {
//     fontSize: 36,
//     marginBottom: 48,
//   },
//   textInput: {
//     height: 40,
//     borderColor: '#000000',
//     borderBottomWidth: 1,
//     marginBottom: 36,
//   },
//   btnContainer: {
//     backgroundColor: 'white',
//     marginTop: 12,
//   },
// });
