import React from 'react';
import {View, StyleSheet,Text,SafeAreaView} from 'react-native';
import tw from 'tailwind-react-native-classnames';
import Map from './Map';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NavigateCard from './NavigateCard';
import RideOptionsCard from './RideOptionsCard';
import { useSelector } from 'react-redux';
import { selectOrigin } from './store/slices/navSlice';
const MapScreen = () => {
    const Stack = createNativeStackNavigator()
    const origin = useSelector(selectOrigin)
    console.log("Origin Changed",origin.location);
    return (
      <View>
        <View style={tw`h-1/2 flex-shrink`}>
            <Map/>
        </View>
       <View style={tw`h-1/2`}>
            <Stack.Navigator>
                <Stack.Screen name="NavigateCard" component={NavigateCard} options={{
                    headerShown:false
                }}/>
                <Stack.Screen name="RideOptionsCard" component={RideOptionsCard} options={{
                    headerShown:false
                }}/>
            </Stack.Navigator>
       </View>
      </View>
    );
}

const styles = StyleSheet.create({})

export default MapScreen;
