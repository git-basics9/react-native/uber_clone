import { StyleSheet, Text, View,FlatList, TouchableOpacity, KeyboardAvoidingView} from 'react-native'
import React from 'react'
import { Icon } from 'react-native-elements'
import tw from 'tailwind-react-native-classnames'
const NavFavourites = () => {
    const data = [
        {
            id:"123",
            title:"Home",
            icon:"home",
            location:"1020/17, Faridabad"
        },
        {
            id:"456",
            title:"Work",
            icon:"work",
            location:"BITCS, Noida"
        }
    ]
  return (
    <View>
      <FlatList
      data={data}
      keyExtractor={(item)=>item.id}
      ItemSeparatorComponent = {()=>(
        <View
        style={[tw`bg-red-200`,{height:0.5}]}/>
      )}
      renderItem={({item})=>(
        <TouchableOpacity style={tw`flex-row items-center p-5`}>
            <Icon
            name={item.icon}
            style={tw`mr-4 p-3 bg-gray-300 rounded-full`}
            color="white"
            />
            <View>
                <Text style={tw`font-bold`}>{item.title}</Text>
                <Text style={tw`text-gray-500`}>{item.location}</Text>
            </View>
        </TouchableOpacity>

      )}
      />  
      </View>
  )
}

export default NavFavourites

const styles = StyleSheet.create({})