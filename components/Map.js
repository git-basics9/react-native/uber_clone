import { StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useRef } from 'react'
import MapView, { Marker } from 'react-native-maps';
import tw from 'tailwind-react-native-classnames';
import MapViewDirections from 'react-native-maps-directions'
import { useSelector } from 'react-redux';
import {selectDestination, selectOrigin} from './store/slices/navSlice'
// import {GOOGLE_MAPS_API_KEY } from '@env';

import {GOOGLE_MAPS_API_KEY} from '@env'
const Map = () => {
    const origin = useSelector(selectOrigin)
    const mapRef = useRef()
    const destination = useSelector(selectDestination)
    useEffect(() => {
        if(!origin || !destination) return;
        console.log("Effect Called");
        mapRef.current.fitToSuppliedMarkers(['origin','destination'])
    }, [origin,destination]);
  return (
    <MapView
    ref={mapRef}
    style={tw`flex-1`}
    mapType="mutedStandard"
    initialRegion={{
      latitude: origin.location.lat,
      longitude: origin.location.lng,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }}
    >
        {origin&&destination&&(
            <MapViewDirections
            origin={origin.description}
            destination={destination.description}
            apikey={GOOGLE_MAPS_API_KEY}
            strokeWidth={3}
            strokeColor="black"
            />
        )}
        {origin?.location && 
        <Marker
        coordinate={{
            latitude: origin.location.lat,
            longitude: origin.location.lng,
        }}
        title="origin"
        description={origin.description}
        identifier="origin"
        />}
        {destination?.location && 
        <Marker
        coordinate={{
            latitude: destination.location.lat,
            longitude: destination.location.lng,
        }}
        title="destination"
        description={origin.description}
        identifier="destination"
        />}
    </MapView>
  )
}
// const styles = StyleSheet.create({})

export default Map
