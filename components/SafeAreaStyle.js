import { StyleSheet ,StatusBar,Platform } from 'react-native';

const SafeArea = StyleSheet.create({

    AndroidSafeArea: {
        flex: 1,
        backgroundColor: "white",
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
      }
   
});

export default SafeArea