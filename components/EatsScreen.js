import React from 'react';
import {View, StyleSheet,Text,SafeAreaView} from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import SafeArea  from './SafeAreaStyle';

const EatsScreen = () => {
    const insets = useSafeAreaInsets();
    return (
        <View style={{
            flex: 1,
            justifyContent: 'space-between',
            alignItems: 'center',
            // Paddings to handle safe area
            paddingTop: insets.top,
            paddingBottom: insets.bottom,
            paddingLeft: insets.left,
            paddingRight: insets.right,
        }}>
            <Text>Eats Screen</Text>
        </View>
    );
}

const styles = StyleSheet.create({})

export default EatsScreen;
