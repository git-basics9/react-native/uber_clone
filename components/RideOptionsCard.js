import { StyleSheet,TouchableOpacity, Text, View, FlatList } from 'react-native'
import {useState} from 'react'
import React from 'react'
import { SafeAreaView } from 'react-native'
import tw from 'tailwind-react-native-classnames'
import { Icon } from 'react-native-elements'
import { useNavigation } from '@react-navigation/native'
import { Image } from 'react-native'
const RideOptionsCard = () => {
  const {navigate} = useNavigation()
  const data = [
    {
      id:"Uber-X-123",
      title:"UberX",
      multiplier:1,
      image:"https://links.papareact.com/3pn"
    },
    {
      id:"Uber-XL-456",
      title:"Uber XL",
      multiplier:1.2,
      image:"https://links.papareact.com/5w8"
    }, 
    {
      id:"Uber-LUX-789",
      title:"Uber LUX",
      multiplier:1,
      image:"https://links.papareact.com/7pf"
    }
  ]
  const [selected, setSelected] = useState({});
  console.log("Clicked on",selected);
  return (
    <SafeAreaView style={tw`bg-white flex-grow`}>
      <View>
        <TouchableOpacity onPress={()=>{navigate('NavigateCard')}}
          style={tw`absolute top-3 left-5 p-2 rounded-full`}>
          <Icon name="chevron-left" type="fontawesome"/>
        </TouchableOpacity>
      <Text style={tw`text-center py-4 text-xl`}>Select a Ride</Text>
      </View>

      <FlatList
      data={data}
      keyExtractor={(item)=>item.id}
      renderItem = {({item})=>(
          
        <TouchableOpacity onPress={()=>setSelected(item)} style={tw`flex-row my-2 justify-between items-center px-10 ${item.id===selected?.id && 'bg-gray-200'}`}>
          <Image
          style={{
            width:50,
            height:50,
            resizeMode: "contain"
          }}
          source={{uri: item.image}}
          />
          <View>
            <Text>{item.title}</Text>
            <Text>Travel Time...</Text>
          </View>
            <Text style={tw`text-xl`}>Rs 100</Text>
        </TouchableOpacity>
      )}
      />
    </SafeAreaView>
  )
}

export default RideOptionsCard