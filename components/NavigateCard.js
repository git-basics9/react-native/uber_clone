import { View, Text, TouchableOpacity, KeyboardAvoidingView } from 'react-native'
import React from 'react'
import { SafeAreaProvider, useSafeAreaInsets } from 'react-native-safe-area-context';
import tw from 'tailwind-react-native-classnames';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { useDispatch } from 'react-redux';
import { setDestination } from './store/slices/navSlice';
import {GOOGLE_MAPS_API_KEY } from '@env';
import { useNavigation } from '@react-navigation/native';
import NavFavourites from './NavFavourites';
import SafeArea from './SafeAreaStyle';
import { Icon } from 'react-native-elements'


const NavigateCard = () => {
    const dispatch = useDispatch()
    const {navigate}  = useNavigation()
  return (
  
    <SafeAreaProvider>
       {/* <View style={{
        flex: 1,
    }}> */}
      <Text style={tw`text-center text-xl py-5 `}>Good Morning, Ishan</Text>
      <View style={tw`border-t border-gray-200 flex-shrink `}>
      <GooglePlacesAutocomplete
            styles={{
            container:{
                flex:0
            },
            textInput:{
                fontSize:18,
                borderRadius:0
            },    
            textInputContainer:{
              paddinghorizontal: 20,
              paddingBottom: 0,

            }
            }}
            fetchDetails={true}
            onPress={(data,details=null)=>{
                // console.log(details.geometry.location);
                dispatch(setDestination({
                    location: details.geometry.location,
                    description: data.description
                }))
                navigate('RideOptionsCard')
            }}
            returnKeyType={"search"}
            enablePoweredByContainer={false} 
            placeholder="Where To?"
            debounce={400}
            minLength={2}
            query={{
                key: GOOGLE_MAPS_API_KEY,
                language: "en"
            }}
            nearbyPlacesAPI="GooglePlacesSearch"
        />
        <NavFavourites/>
    </View>
      {/* </View> */}
      <View style={tw`my-5 flex-row justify-evenly py-2 mt-auto border-t border-gray-100`}>
        <TouchableOpacity style={tw`flex flex-row bg-black w-24 px-4 py-3 rounded-full`}
        onPress={()=>navigate('RideOptionsCard')}>
            <Icon name="car" type="font-awesome" color="white" size={16}/>
            <Text style={tw`text-white text-center mx-2`}>Rides</Text>
        </TouchableOpacity>
        
        <TouchableOpacity style={tw`flex flex-row bg-black w-24 px-4 py-3 rounded-full`}
        onPress={()=>navigate('EatsScreen')}>
            <Icon name="car" type="font-awesome" color="white" size={16}/>
            <Text style={tw`text-white text-center mx-2`}>Eats</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaProvider>
  )
}

export default NavigateCard