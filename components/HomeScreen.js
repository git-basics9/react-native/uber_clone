import React from 'react';
import {View, StyleSheet,Image, SafeAreaView, KeyboardAvoidingView} from 'react-native';
import tw from 'tailwind-react-native-classnames';
import Navoptions from './Navoptions';
import SafeArea from './SafeAreaStyle';
import {GOOGLE_MAPS_API_KEY } from '@env';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { useDispatch } from 'react-redux';
import { setOrigin,setDestination } from './store/slices/navSlice';
import NavFavourites from './NavFavourites';
const HomeScreen = () => {
    const dispatch = useDispatch()
    return (
        <View style={[tw`my-5 mx-5`,{justifyContent:'space-around'}]}>
            <Image
                style={{width:100,height:100,resizeMode:"contain"}} 
                source={{
                    uri: 'https://links.papareact.com/gzs'
                }}
            />
        <GooglePlacesAutocomplete
            styles={{
            container:{
                flex:0
            },
            textInput:{
                fontSize:19,
            },    
            }}
            fetchDetails={true}
            onPress={(data,details=null)=>{
                // console.log(details.geometry.location);
                dispatch(setOrigin({
                    location: details.geometry.location,
                    description: data.description
                }))
                dispatch(setDestination(null)) //by default it is null but if we go back and forth and change our origin then it should again set the destination as null
            }}
            returnKeyType={"search"}
            enablePoweredByContainer={false} 
            placeholder="Where From?"
            debounce={400}
            minLength={2}
            query={{
                key: GOOGLE_MAPS_API_KEY,
                language: "en"
            }}
            nearbyPlacesAPI="GooglePlacesSearch"
        />
            <Navoptions/>   
            <NavFavourites/>
        </View>
    );  
}

const styles = StyleSheet.create({});

export default HomeScreen;
