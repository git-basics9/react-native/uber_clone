import React from 'react';
import { useNavigation } from '@react-navigation/native';
import {View, StyleSheet, Text, FlatList, TouchableOpacity, Image, KeyboardAvoidingView} from 'react-native';
import tw from 'tailwind-react-native-classnames';
import { useSelector } from 'react-redux';
import { selectOrigin } from './store/slices/navSlice';

const Navoptions = () => {
    const {navigate} = useNavigation()
    const origin = useSelector(selectOrigin) //now the component will re render whenever our origin selector changes
    console.log("Re-Rendered",origin);
    const data = [
        {
            id:'123',
            title:"Get a Ride",
            image:"https://www.uber-assets.com/image/upload/f_auto,q_auto:eco,c_fill,w_558,h_372/v1568070387/assets/b5/0a5191-836e-42bf-ad5d-6cb3100ec425/original/UberX.png",
            screen:'MapScreen'
        },
        {
            id:'456',
            title:"Order Food",
            image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQvy6QOO5DTCznN3TdIp5DkT9R67sx4OlNjtA&usqp=CAU',
            screen:'EatsScreen'
        }

    ]
    return (
        <View>
           <FlatList
            data={data}
            horizontal //it is vertical by default
            keyExtractor={(item)=>item.id}
            renderItem={({item})=>(
                <TouchableOpacity
                disabled={!origin}
                onPress={()=>navigate(item.screen)}
                style={tw`p-2 pl-6 pb-8 pt-4 bg-white m-2 mt-5 w-40 rounded-xl`}>
                    <View style={tw`${!origin && 'opacity-20'}`}>
                    <Image style={{width:80,height:80,resizeMode:'contain'}} source={{uri: item.image}}/>
                    <Text style={tw` text-lg font-semibold`}>{item.title}</Text>
                    </View>
                </TouchableOpacity>
        )}
           />
        </View>
    );
}

const styles = StyleSheet.create({})

export default Navoptions;
